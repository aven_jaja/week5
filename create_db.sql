-- User details
create table account (
    username varchar(255) not null primary key,
    password varchar(255),
    fullname varchar(255) not null,
    email varchar(255) not null,
    dob date not null
);

-- User view used by JDBC Realm
create view jdbcrealm_user (username, password) as
select username, password
from account;

-- Group view used by JDBC Realm
-- This view ensure every account belongs in the 'Users' group
create view jdbcrealm_group (username, groupname) as
select username, 'Users'
from account;

-- Sample accounts
-- The passwords come from a list of top-ten passwords
insert into account (username, password, fullname, email, dob) values
    ('cbrady', 'password', 'Carol Brady', 'cbrady@example.com', {d '1934-02-14'}),
    ('mbrady', 'qwerty', 'Mike Brady', 'mbrady@example.com', {d '1932-10-19'}),
    ('anelson', '123456', 'Alice Nelson', 'anelson@example.com', {d '1926-05-05'}),
    ('sfranklin', 'iloveyou', 'Sam Franklin', 'sfranklin@example.com', {d '1923-02-18'});

-- More sample accounts, for container-managed authentication
-- The passwords are the same as above, but have been encrypted using SHA-256
insert into account (username, password, fullname, email, dob) values
    ('cbrady2', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'Carol Brady', 'cbrady@example.com', {d '1934-02-14'}),
    ('mbrady2', '65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5', 'Mike Brady', 'mbrady@example.com', {d '1932-10-19'}),
    ('anelson2', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Alice Nelson', 'anelson@example.com', {d '1926-05-05'}),
    ('sfranklin2', 'e4ad93ca07acb8d908a3aa41e920ea4f4ef4f26e7f86cf8291c5db289780a5ae', 'Sam Franklin', 'sfranklin@example.com', {d '1923-02-18'});
