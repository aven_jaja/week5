package au.edu.uts.aip.accounts;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.enterprise.context.*;
import javax.faces.application.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.*;

/**
 * A backing bean used for account management in our JavaServer Faces application.
 */
@Named
@RequestScoped
public class AccountController {

    // Properties of the Java Bean, used by JSF fields
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Get a list of all users in the database.
     * @return a list of all users in the database
     * @throws DataStoreException if there is an error accessing the database
     */
    public ArrayList<AccountDTO> getAllUsers() throws DataStoreException {
        return new AccountDAO().findAll();
    }
    
    /**
     * Attempt to log in using the username/password properties set by JSF.
     * Uses the AccountDAO to perform the password check.
     * @return an outcome corresponding to an insecure welcome page, null if the login failed
     * @throws DataStoreException if there is an error accessing the database
     */
    public String loginDao() throws DataStoreException {
        AccountDAO dao = new AccountDAO();
        AccountDTO user = dao.findUser(username);
        if (password.equals(user.getPassword())) {
            return "/welcome_not_secure?faces-redirect=true";
        } else {
            showError("Incorrect username or password");
            return null;
        }
    }
    
    /**
     * Attempt to log in using the username/password properties set by JSF.
     * Uses the container-managed authentication to perform the password check.
     * @return an outcome corresponding to a secure welcome page, null if the login failed
     */
    public String loginContainer() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        
        try {
          request.login(username, password);
          return "/welcome?faces-redirect=true";
          
        } catch (ServletException e) {
          showError("Incorrect username or password (or you may not have properly configured aipRealm)");
          e.printStackTrace();
          return null;
        }
    }
    
    /**
     * Logs out the current user of the container-managed authentication.
     * @return an outcome corresponding to the login page
     * @throws ServletException if there is no currently logged in user
     */
    public String logoutContainer() throws ServletException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        request.logout();
        return "/login?faces-redirect=true";
    }

    /**
     * Dumps the entire database to the server log.
     * Uses JNDI (i.e., container managed connection pools) to establish a database connection.
     * @throws SQLException if there is an error when communicating with the database
     * @throws NamingException if the JDBC resource "jdbc/aip" can not be found
     */
    public void listUsersJNDI() throws SQLException, NamingException {
        String query = "select username, password, fullname, email, dob " +
                       "from account";
        
        Logger log = Logger.getLogger(this.getClass().getName());
        DataSource ds = InitialContext.doLookup("jdbc/aip");
        
        try (Connection conn = ds.getConnection();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(query)) {

            log.info("The accounts table contains:");
            while (rs.next()) {
                log.info("username = " + rs.getString("username") +
                         ", password = " + rs.getString("password") +
                         ", fullname = " + rs.getString("fullname") +
                         ", email = " + rs.getString("email") +
                         ", dob = " + rs.getDate("dob"));
            }
            log.info("End of accounts table.");
        }
    }
    
    /**
     * Dumps the entire database to the server log.
     * Uses JDBC to establish a database connection directly.
     * <p>
     * Note that this is bad practice! You should always use container-managed connections when working in Java EE.
     * @throws SQLException if there is an error when communicating with the database
     */
    public void listUsersJDBC() throws SQLException {
        String connectionString = "jdbc:derby://localhost:1527/aip";
        String dbUsername = "aip";
        String dbPassword = "aip";
        String query = "select username, password, fullname, email, dob " +
                       "from account";
        
        Logger log = Logger.getLogger(this.getClass().getName());
        
        try (Connection conn = DriverManager.getConnection(connectionString, dbUsername, dbPassword);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(query)) {
            
            log.info("The accounts table contains:");
            while (rs.next()) {
                log.info("username = " + rs.getString(1) +
                         ", password = " + rs.getString(2) +
                         ", fullname = " + rs.getString(3) +
                         ", email = " + rs.getString(4) +
                         ", dob = " + rs.getDate(5));
            }
            log.info("End of accounts table.");
        }
    }
    
    /**
     * Adds a message to the current faces context, so that it will appear in
     * a h:messages element.
     * @param message the text of the error message to show the user
     */
    private void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
}
